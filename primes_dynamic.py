import sys

from tools import print_elapse_time


def filter_prime_dynamic():
    primes = [2]
    for x in range(3, 2**30):
        for p in primes:
            if x % p == 0:
                break
        else:
            primes.append(x)
            if len(primes) == 10000:
                return primes[-1]


print(sys.version)
with print_elapse_time():
    print(filter_prime_dynamic())
