#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct gen gen;
struct gen {
    int (*next)(void *this);
};

typedef struct count count;
struct count {
    gen base;
    int i;
};
int count_next(count *this) {
    return this->i++;
}
void count_init(count *this, int start) {
    this->base.next = (void*)count_next;
    this->i = start;
}

typedef struct saturate saturate;
struct saturate {
    gen base;
    int max;
    gen *from;
};
int saturate_next(saturate *this) {
    int v = this->from->next(this->from);
    if(v > this->max)
        return this->max;
    else
        return v;
} 
void saturate_init(saturate *this, gen *from, int max) {
    this->base.next = (void*)saturate_next;
    this->from = from;
    this->max = max;
}

void test_saturate_count() {
    count c; count_init(&c, 2);
    saturate s; saturate_init(&s, (gen*)&c, 3);
    gen *g = (gen*)&s;
    printf("%d\n", g->next(g));
    printf("%d\n", g->next(g));
    printf("%d\n", g->next(g));
    printf("%d\n", g->next(g));
}

typedef struct not_mult not_mult;
struct not_mult {
    gen base;
    gen *xs;
    int p;    
};
int not_mult_next(not_mult *this) {
    int x = this->xs->next(this->xs);
    while(x % this->p == 0)
        x = this->xs->next(this->xs);
    return x;
}
void not_mult_init(not_mult *this, gen *xs, int p) {
    this->base.next = (void*)not_mult_next;
    this->xs = xs;
    this->p = p;
}

typedef struct filter_prime filter_prime;
struct filter_prime {
    gen base;
    gen *xs, *subset;
    filter_prime *primes; 
    bool first;
};
void filter_prime_init(filter_prime *this, gen *xs);
int filter_prime_next(filter_prime *this) {
    if(this->first) { this->first = false;
        int prime = this->xs->next(this->xs);
        this->subset = malloc(sizeof(not_mult));
        not_mult_init((not_mult*)this->subset, this->xs, prime);
        this->primes = malloc(sizeof(filter_prime));
        filter_prime_init((filter_prime*)this->primes, this->subset);
        return prime;
    }
    return filter_prime_next(this->primes);
}
void filter_prime_init(filter_prime *this, gen *xs) {
    this->base.next = (void*)filter_prime_next;
    this->xs = xs;
    this->primes = NULL;
    this->subset = NULL;
    this->first = true;
}
void filter_prime_destroy(filter_prime *this) {
    if(this->primes != NULL) {
        filter_prime_destroy(this->primes);
        free(this->primes);
    }
    if(this->subset != NULL) free(this->subset);
}

void prime_10000th() {
    int prime;
    count c; count_init(&c, 2);
    filter_prime primes; filter_prime_init(&primes, (gen*)&c);
    gen *g = (gen*)&primes;
    for(int i=0; i<10000; i++) {
        prime = g->next(g);
        // prime = primes.base.next(&primes);
    }
    filter_prime_destroy(&primes);
    printf("%d\n", prime);
}

int main() {
    // test_saturate_count();
    prime_10000th();
    return 0;
}
