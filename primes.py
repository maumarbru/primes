import sys
from itertools import count

from tools import print_elapse_time, iget


def filter_prime(xs=count(2)):
    p = next(xs)
    yield p; yield from filter_prime(x for x in xs if x % p != 0)


primes = filter_prime()

sys.setrecursionlimit(100000)
print(sys.version)
with print_elapse_time():
    print(iget(primes, 10000))
