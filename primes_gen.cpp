#include <iostream>
using namespace std;

struct Gen {
    virtual int next() = 0;
    virtual ~Gen() {}
    class Stop : std::exception {};
};

struct range : Gen {
    range(int start, int end) : i(start), end(end) {}
    int next() {
        if(i >= end) throw Gen::Stop();
        return i++;
    }
private: int i, end;
};

struct count : Gen {
    count(int start=0) : i(start) {}
    int next() {
        return i++;
    }
private: int i;
};

struct not_mult : Gen {
    not_mult(Gen &xs, int &p) : xs(xs), p(p) {}
    int next() {
        int x = xs.next();
        while(x % p == 0)
            x = xs.next();
        return x;
    }
private: Gen &xs; int &p;
};

struct filter_prime : Gen {
    filter_prime(Gen &xs) : xs(xs) {}
    int next() {
        if(first) { first = false;
            int prime = xs.next();
            subset = new not_mult(xs, prime);
            primes = new filter_prime(*subset);
            return prime;
        }
        return primes->next();
    }
    virtual ~filter_prime() {
        if(primes != nullptr) delete primes;
        if(subset != nullptr) delete subset;
    }
private: Gen &xs, *primes, *subset; bool first=true;
};

void prime_10000th() {
    int prime;
    count two_to_infinity(2);
    auto primes = filter_prime(two_to_infinity);
    for(int i=0; i<10000; i++)
        prime = primes.next();
    cout << prime << endl;
}

void last_prime_lt_100000() {
    int prime;
    range two_to_n(2, 100000);
    filter_prime primes(two_to_n);
    try {
        for(;;) prime = primes.next();
    }
    catch(Gen::Stop) {
        cout << prime << endl;
    }
}

int main() {
    // last_prime_lt_100000();
    prime_10000th();
}
