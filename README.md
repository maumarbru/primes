# primes

## Haskell

This is when I starting trying, with the beauty ;)

[primes.hs](primes.hs)

```haskell
primes = filterPrime [2..]
  where filterPrime (p:xs) =
          p : filterPrime [x | x <- xs, mod x p /= 0]

main = print (last (take 10000 primes))
```

```sh
docker run -it --rm -v $PWD:/app -w /app haskell ghc primes.hs -O -o primes_hs
```

## Python

This is the more similar form of the haskell expression I could create in Python:
```python
def filter_prime(xs=count(2)):
    p = next(xs)
    yield p; yield from filter_prime(x for x in xs if x % p != 0)
```

[primes.py](primes.py)

```sh
python3 primes.py
```

### A more performant version using dynamic programing
```python
def filter_prime_dynamic():
    primes = [2]
    for x in range(3, 2**30):
        for p in primes:
            if x % p == 0:
                break
        else:
            primes.append(x)
            if len(primes) == 10000:
                return primes[-1]
```

[primes_dynamic.py](primes_dynamic.py)


The following script will show times running with plain CPython and also using `numba`:
```sh
python3 primes_dyn_numba.py
```
[primes_dyn_numba.py](primes_dyn_numba.py)


### pypy

Using the same [primes_dynamic.py](primes_dynamic.py)

```sh
docker run --rm -it -v $PWD/primes_dynamic.py:/p.py:ro pypy pypy3 /p.py
```

## C/C++

gcc version 8.3.0 (Debian 8.3.0-6)

Linux satemoon 4.19.0-6-amd64 #1 SMP Debian 4.19.67-2 (2019-08-28) x86_64 GNU/Linux

### C dynamic version

[primes_dyn.c](primes_dyn.c)

```sh
gcc -O2 primes_dyn.c -o primes_dyn_c
```

### C++ version using a pseudo generators

[primes_gen.cpp](primes_gen.cpp)

```sh
g++ -O2 primes_gen.cpp -o primes_gen_cpp
```

### C version using a pseudo generators and implementing OOP and RTTI

[primes_gen.c](primes_gen.c)

```sh
gcc -O2 primes_gen.c -o primes_gen_c
```

## Benchmarks
```sh
$ time ./primes_hs
104729

real    0m2.802s
user    0m2.783s
sys 0m0.017s
```

```sh
$ python3 primes.py
3.7.3 (default, Apr  3 2019, 05:39:12) 
[GCC 8.3.0]
104729
23.529 seconds.
```

```sh
$ python3 primes_dyn_numba.py
3.7.3 (default, Apr  3 2019, 05:39:12) 
[GCC 8.3.0]
104729
2.801 seconds.

with numba (first call):
104729
0.383 seconds.

with numba (seccond call):
104729
0.257 seconds.
```

```sh
$ docker run --rm -it -v $PWD/primes_dynamic.py:/p.py:ro pypy pypy3 /p.py
3.5.3 (928a4f70d3de, Feb 08 2019, 10:42:58)
[PyPy 7.0.0 with GCC 6.2.0 20160901]
104729
0.708 seconds.
```

```sh
$ time ./primes_dyn_c
104729

real    0m0.226s
user    0m0.223s
sys 0m0.001s
```

```sh
$ time ./primes_gen_cpp
104729

real    0m0.784s
user    0m0.782s
sys 0m0.000s
```

```sh
$ time ./primes_gen_c
104729

real    0m0.729s
user    0m0.724s
sys 0m0.004s
```