from primes_dynamic import *
import numba

filter_prime_dynamic = numba.jit(nopython=True)(filter_prime_dynamic)

print("\nwith numba (first call):")
with print_elapse_time():
    print(filter_prime_dynamic())

print("\nwith numba (seccond call):")
with print_elapse_time():
    print(filter_prime_dynamic())
