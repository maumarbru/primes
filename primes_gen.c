#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct gen gen;

typedef struct gen_vtbl gen_vtbl;
struct gen_vtbl {
    int (*next)(void *this);
};

struct gen {
    gen_vtbl *v;
};


typedef struct count count;
typedef struct count_vtbl count_vtbl;
struct count_vtbl {
    int (*next)(count *this);
};
struct count {
    union {
        count_vtbl *v;
        gen base;
    };
    int i;
};
int count_next(count *this) {
    return this->i++;
}
count_vtbl count_vtbl2 = {
    .next = count_next,
};
void count_init(count *this, int start) {
    this->v = &count_vtbl2;
    this->i = start;
}
#define count_create(name, start) count name; count_init(&name, start)

typedef struct {
    gen base;
    int max;
    gen *from;
} saturate;
int saturate_next(saturate *this) {
    gen * const from = this->from;
    int v = from->v->next(from);
    if(v > this->max)
        return this->max;
    else
        return v;
} 
gen_vtbl saturate_vtbl = {
    .next = (void*)saturate_next,
};
void _saturate_init(saturate *this, gen *from, int max) {
    this->base.v = &saturate_vtbl;
    this->from = from;
    this->max = max;
}
#define saturate_init(name, from, max) saturate name; _saturate_init(&name, (gen*)&from, max)


int test_saturate_count() {
    count_create(c, 2);
    saturate_init(s, c, 3);
    gen *g = (gen*)&s;
    printf("%d\n", g->v->next(g));
    printf("%d\n", g->v->next(g));
    printf("%d\n", g->v->next(g));
    printf("%d\n", g->v->next(g));
    return 0;
}


typedef struct not_mult not_mult;
struct not_mult {
    gen base;
    gen *xs;
    int p;    
};
int not_mult_next(not_mult *this) {
    int x = this->xs->v->next(this->xs);
    while(x % this->p == 0)
        x = this->xs->v->next(this->xs);
    return x;
}
gen_vtbl not_mult_vtbl = {
    .next = (void*)not_mult_next,
};
void not_mult_init(not_mult *this, gen *xs, int p) {
    this->base.v = &not_mult_vtbl;
    this->xs = xs;
    this->p = p;
}


typedef struct filter_prime filter_prime;
typedef struct filter_prime_vtbl filter_prime_vtbl;
struct filter_prime_vtbl {
    int (*next)(filter_prime *this);
    void (*destroy)(filter_prime *this);
};
struct filter_prime {
    union {
        filter_prime_vtbl *v;
        gen base;
    };
    gen *xs, *subset;
    filter_prime *primes; 
    bool first;
};
void filter_prime_init(filter_prime *this, gen *xs);
int filter_prime_next(filter_prime *this) {
    if(this->first) { this->first = false;
        int prime = this->xs->v->next(this->xs);
        this->subset = malloc(sizeof(not_mult));
        not_mult_init((not_mult*)this->subset, this->xs, prime);
        this->primes = malloc(sizeof(filter_prime));
        filter_prime_init((filter_prime*)this->primes, this->subset);
        return prime;
    }
    return filter_prime_next(this->primes);
}
void filter_prime_destroy(filter_prime *this) {
    if(this->primes != NULL) {
        filter_prime_destroy(this->primes);
        free(this->primes);
    }
    if(this->subset != NULL) free(this->subset);
}
filter_prime_vtbl filter_prime_vtbl2 = {
    .next = filter_prime_next,
    .destroy = filter_prime_destroy,
};
void filter_prime_init(filter_prime *this, gen *xs) {
    this->v = &filter_prime_vtbl2;
    this->xs = xs;
    this->primes = NULL;
    this->subset = NULL;
    this->first = true;
}
#define filter_prime_create(name, from) filter_prime name; filter_prime_init(&name, (gen*)&from)

#define gen_ptr(name, to) gen *name = (gen*)&to

#define _(method, name) name.v->method(&name)
#define _p(method, pname) pname->v->method(pname)

void prime_10000th() {
    int prime;
    count_create(c, 2);
    filter_prime_create(primes, c);
    gen_ptr(g, primes);
    for(int i=0; i<10000; i++) {
        // prime = _p(next, g);
        prime = _(next, primes);
        // prime = g->v->next(g);
    }
    // primes.v->destroy(&primes);
    _(destroy, primes);
    printf("%d\n", prime);
}

int main() {
    // test_saturate_count();
    prime_10000th();
    return 0;
}