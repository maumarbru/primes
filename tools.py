import time

from contextlib import contextmanager


@contextmanager
def print_elapse_time():
    t = time.monotonic()
    try:
        yield
    finally:
        print(f"{time.monotonic() - t:.3f} seconds.")


def iget(iterable, count):
    for _ in range(count):
        v = next(iterable)
    return v
