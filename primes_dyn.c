#include <stdio.h>

int primes_for_index() {
    int primes[10000];
    int i=0;
    primes[i] = 2;
    for(int x=3; i<10000; x++) {
        int j;
        for(j=0; j<i; j++)
            if(x % primes[j] == 0) break;
        if(j>=i) {
            primes[i] = x;
            i++;
        }
    }
    return primes[9999];
}

int main() {
    int p = primes_for_index();
    printf("%d\n", p);
    return 0;
}
